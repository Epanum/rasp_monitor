var monitor = monitor ||  {};

const listStoragekey = "rasp_monitor_list";

monitor.store = {
    data: {
        list: function() {
            var val = $.cookie(listStoragekey);

            if (!val){
                return [
                    {
                        url: "http://mobilsiden.dk",
                        displayTime: 2,
                        refreshRate: 4
                    },
                    {
                        url: "http://www.hesehus.dk",
                        displayTime: 3,
                        refreshRate: 2
                    }
                ];
            }

            return JSON.parse(val);
        },
        add: function(url, displayTime, refreshRate) {
            var data = JSON.parse($.cookie(listStoragekey)) || [];
            data.add({url: url, displayTime: displayTime, refreshRate: refreshRate});
            $.cookie(listStoragekey, JSON.stringify(data));
            //localStorage.setItem(listStoragekey, JSON.stringify(data));
            return;
        },
        remove: function(idx) {
            var data = JSON.parse($.cookie(listStoragekey)) || [];
            data = data.splice(idx, 1);
            $.cookie(listStoragekey, JSON.stringify(data));
            //localStorage.setItem(listStoragekey, JSON.stringify(data));
            return;
        },
        update: function(idx, url, displayTime, refreshRate) {
            var data = JSON.parse($.cookie(listStoragekey)) || [];
            data = data.remove(idx, 1);
            data.add({url: url, displayTime: displayTime, refreshRate: refreshRate});
            $.cookie(listStoragekey, JSON.stringify(data));
            //localStorage.setItem(listStoragekey, JSON.stringify(data));
            return;
        },
        updatelist: function(list) {
            $.cookie(listStoragekey, JSON.stringify(list));
            //localStorage.setItem(listStoragekey, JSON.stringify(list));
            return;
        }
    }
};