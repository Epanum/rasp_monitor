var monitor = monitor ||  {};

monitor.display = {
    init: function() {
        var container = $('#iframeContainer');

        var items = [];

        var list = monitor.store.data.list();

        var viewportHeight = $(window).height();

        var resize = function (){
            var viewportHeight = $(window).height();

            var adjust = $('nav').is(":visible") ? 56 : 6;

            $('iframe').height(viewportHeight - adjust);
        }

        $(window).resize(resize);

        var startInterval = 0;

        list.forEach(function(element, idx) {
            var div = $('<div>');
            div.addClass('item');

            div.attr({
                'data-display': element.displayTime,
                'data-refresh': element.refreshRate,
                'data-time': + new Date()
            });

            if (idx == 0){
                div.addClass('active');
                startInterval = element.displayTime;
            }

            var iframe = $('<iframe>');
            iframe.attr({
                width: '100%',
                height: viewportHeight - 56,
                src: element.url,
                style: 'border: none;overflow: hidden;'
            });

            div.append(iframe);

            container.append(div);
        }, this);

        var carousel = $('#myCarousel');

        carousel.carousel({
            interval:false
        });

        setTimeout(function() {
            carousel.carousel('next');
        }, startInterval * 1000)

        carousel.on('slide.bs.carousel', function (e) {
            carousel.carousel('pause');
            var item = $(e.relatedTarget);
            var newInterval = item.attr('data-display');

            var shown = item.attr('data-time');
            var now = + new Date();
            
            var secAgo = Math.floor((now - shown) / 1000);
            var diff = secAgo > item.data('refresh');
            if (diff) {
                var iframe = item.find('iframe');
                
                iframe.attr('src', iframe.attr('src'));

                item.attr('data-time', now);
            }

            carousel.carousel('pause');
            console.log('show for: ' + newInterval * 1000)
            setTimeout(function() {
                carousel.carousel('next');
            }, newInterval * 1000)
        });

        $(document).on('keydown', function(e) {
            if ( e.ctrlKey && e.altKey && (e.which == 70)) {
                console.log( "You pressed CTRL + ALT " + e.which );

                var nav = $('nav');

                if (nav.is(":visible")) {
                    nav.hide();
                    $('#myCarousel').css('padding-top','0px')
                } else {
                    nav.show();
                    $('#myCarousel').css('padding-top','50px')
                }

                resize();
              }
        });
    }
}

$(monitor.display.init());