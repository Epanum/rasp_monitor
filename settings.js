var monitor = monitor ||  {};

monitor.settings = {
    init: function () {
        monitor.settings.updatelist();
        $("#save-table").on('click', monitor.settings.save);
    },
    save: function() {
        var rows = $('#listContainer tr');

        var data = [];
        rows.each(function(idx, tr) {
            var row = {};
            $(tr).find('td input').each(function(idx, item){
                var i = $(item);
                row[i.data('name')] = i.val();

                var ov = i.data('original-val');

                if (ov != i.val()){
                    i.css('border-color', 'lightgreen');
                    setTimeout(function() {
                        i.css('border-color', '');
                    }, 500);
                };

                i.data('original-val', i.val());
            });
            data.push(row);
        });

        monitor.store.data.updatelist(data);
        $("#save-table").hide();
    },
    updatelist: function() {
        var listContainer = $('#listContainer');

        var list = monitor.store.data.list();

        function createRow(idx){
            return $('<tr data-idx="' + idx + '">');
        };

        function createTd(row, name, text, addEditListener, editable) {
            var elm = $('<td>');
            var div = $('<div>');

            if (editable) {
                var txt = $('<input>');
                txt.val(text);
                txt.addClass('form-control validate-change');
                txt.attr({
                    'data-original-val': text,
                    'data-name': name
                });
                div.append(txt);
            }
            else {
                div.text(text);
            }
            
            elm.append(div);
            row.append(elm);

            if (addEditListener) {
                div.on("keyup", "input", function() {
                    var elm = $(this);
                    if (elm.data('original-val') !== elm.val()) {
                        $("#save-table").show();
                    } else {
                        $("#save-table").hide();
                    }
                });
            }
            return row;
        };

        console.log(list);
        list.forEach(function(element, idx) {
            var row = createRow(idx);
            listContainer.append(createTd(row, 'url', element.url, true, true));
            listContainer.append(createTd(row, 'displayTime', element.displayTime, true, true));
            listContainer.append(createTd(row, 'refreshRate', element.refreshRate, true, true));
        }, this);
    }
};

(function() {
    monitor.settings.init();
})();